## DAQ Setup Files

### pcdmre02: bcoool-online service

```
systemd-analyze verify bcoool-online.service
cp bcoool-online.service /etc/systemd/system/
systemctl daemon-reload
systemctl enable bcoool-online
systemctl start bcoool-online

systemctl status bcoool-online
journalctl -u bcoool-online

systemctl stop bcoool-online
```

### pcdmre02: bcoool-dump service

```
cp bcoool-dump.service /etc/systemd/system/
cp bcoool-dump.timer /etc/systemd/system/
systemctl daemon-reload
systemctl enable bcoool-dump.timer
systemctl start bcoool-dump.timer
systemctl start bcoool-dump.service

systemctl list-timers bcoool-dump.timer
systemctl status bcoool-dump.timer bcoool-dump.service
```
