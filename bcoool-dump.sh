#!/bin/bash -e

OUTPUTDIR=/online/shift
COOOL=/online/soft/p348-daq/coral/src/coool
REVIEW=/online/soft/scripts/StartReview.sh
WWW=/online/sys/www/trendview/cache

#OUTPUTDIR=$(pwd)/shift
#COOOL=$(pwd)/../p348-daq/coral/src/coool

echo "INFO: OUTPUTDIR=$OUTPUTDIR"
test -d $OUTPUTDIR

source /online/soft/root/root_v6.24.06/bin/thisroot.sh

workd=$(mktemp --tmpdir -d bcoool-dump.XXXX)
cd $workd

# run cleanup on exit
workdfull=$(pwd)
trap "rm -rf $workdfull" EXIT

echo "INFO: working directory: $(pwd)"

find $OUTPUTDIR -type f -name "*.root" | \
while read a ; do
  echo ${a//*-enddate} $a
done | \
sort | \
while read b c ; do
  rm -f dump.txt
  root -b -q -l "$COOOL/dumpcoool.cxx(\"$c\")"
  cat dump.txt >> dump-all.txt
done

mv -f dump-all.txt $OUTPUTDIR/dump.txt

cd
rm -rf $workd

echo "INFO: db dump update finished"
pwd
ls -lh $OUTPUTDIR/dump.txt

mkdir -p $WWW
cd $WWW
for i in eff led led-ecal0 led-ecal1 led-hcal led-vhcal mm12 xy scalers calo ; do
  $REVIEW $i ndays=2
  mv trendview.png trendview-$i.png
done

# last point metadata: run spill time
tail -n1 $OUTPUTDIR/dump.txt | cut -d' ' -f 1-3 > trendview.txt

cd

echo "INFO: www pages update finished"

echo "INFO: processing finished"
