#!/bin/bash -e

INPUT=@pcdmre01:
OUTPUTDIR=/online/shift
P348=/online/soft/p348-daq
COOOL=$P348/coral/src/coool
CFGFILESDIR=$COOOL/monitor

echo "INFO: INPUT=$INPUT, OUTPUTDIR=$OUTPUTDIR"

time0=$(date +%Y%m%dT%H%M%S)
workd=$(mktemp --tmpdir -d bcoool-$time0.XXXX)
cd $workd

# run cleanup on exit
workdfull=$(pwd)
trap "rm -rf $workdfull" EXIT

echo "INFO: working directory: $(pwd)"

export CONDDB=$P348/p348reco/conddb
$COOOL/src/bcoool \
  -map $P348/maps \
  -group $COOOL/monitor/groups.xml \
  -geom $COOOL/lstrack/detectors.dat \
  -root bcoool.root \
  -text bcoool.txt \
  -notree -experthistos \
  -ps $CFGFILESDIR \
  -stopatendrun \
  $INPUT
  
# currently, the generated monitor.pdf by the option `-pdf` is broken
#bcoool ...  -pdf $CFGFILESDIR

echo "INFO: bcoool finished"
pwd
ls -l

# process generated data

# note, generated .ps is problematic, but the latter `ps2pdf` conversion will produce good .pdf file
# keep the same timestamp for .root and .ps
touch -r bcoool.root monitor.ps

# prepare run ID
runnumber=$(grep ^runnumber= bcoool.txt | cut -d= -f2)
timestamp=$(grep ^timestamp= bcoool.txt | cut -d= -f2)
rm -f bcoool.txt

# convert unix timestamp to readable string
enddate=$(date -d @$timestamp +%Y%m%dT%H%M%S)

runID=run$runnumber-enddate$enddate

echo "INFO: runID = $runID"

# copy data to destination
mv -v -f bcoool.root $OUTPUTDIR/$runID.root
mv -v -f monitor.ps $OUTPUTDIR/$runID.ps

echo "INFO: processing finished"
